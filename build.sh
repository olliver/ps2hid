#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2020 Olliver Schinagl <oliver@schinagl.nl>

set -eu

DEF_BUILD_TARGET="atmega32u4"
DEF_KEEP_BUILD_CACHE="true"
DEF_OUTPUT_DIR="output"

e_err() {
	echo >&2 "ERROR: ${*}"
}

e_warn() {
	echo "WARN: ${*}"
}

usage() {
	echo "Uage: ${0} [OPTIONS] [COMMAND]"
	echo "This is a wrapper script around the included Makefile, calling make with [COMMAND]."
	echo "Its purpose is to make setting of build dirs and passing of configurations easier."
	echo "    -b  Build target to use (default: ${DEF_BUILD_TARGET}) [BUILD_TARGET]"
	echo "    -c  Clear previous build cache (default: ${DEF_KEEP_BUILD_CACHE}) [KEEP_BUILD_CACHE]"
	echo "    -h  Print usage"
	echo "    -o  Set the output directory (default: '${DEF_OUTPUT_DIR}/\${ARCH}/\${BUILD_CONFIG}') [OUTPUT_DIR]"
	echo "    -x  Override the target compiler, an empty var performs a native build (default: avr-). [CROSS_COMPILE]"
	echo
	echo "All options can also be passed in environment variables (listed between [brackets])."
}

init() {
	trap cleanup EXIT

	if [ -z "${MAKEFLAGS:-}" ]; then
		MAKEFLAGS="-j$(($(nproc) - 1))"
		e_warn "MAKEFLAGS where not set, setting them to '${MAKEFLAGS}'."
		export MAKEFLAGS
	fi

	if [ "${keep_build_cache}" != "true" ] &&
	   [ -d "${build_dir}" ]; then
		rm -rf "${build_dir:?}"
	fi
	mkdir -p "${build_dir}"

	if [ -z "${LOCALVERSION:-}" ] && git rev-parse --show-cdup 2> "/dev/null"; then
		if ! git describe 2> "/dev/null"; then
			LOCALVERSION="g"
		fi
		LOCALVERSION="-${LOCALVERSION:-}$(git describe --always --dirty)"
		export LOCALVERSION
	fi

	if [ -d "${output_dir}" ]; then
		rm -rf "${output_dir:?}"
	fi
	mkdir -p "${output_dir}"
}

cleanup() {
	if [ "${keep_build_cache}" != "true" ] &&
	   [ -d "${build_dir}" ]; then
		rm -rf "${build_dir:?}"
	fi

	trap EXIT
}

build_project() {
	CROSS_COMPILE="${cross_compile}" \
	nice -n 19 make \
	DEVICE="${build_target}" \
	INSTALL_PATH="${output_dir}" \
	O="${build_dir}" \
	"${@}"
}

main() {
	_start_time="$(date "+%s")"

	while getopts ":b:cho:x:" _options; do
		case "${_options}" in
		b)
			build_target="${OPTARG}"
			;;
		c)
			keep_build_cache="false"
			;;
		h)
			usage
			exit 0
			;;
		o)
			output_dir="${OPTARG}"
			;;
		x)
			cross_compile="${OPTARG}"
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	build_target="${build_target:-${BUILD_TARGET:-${DEF_BUILD_TARGET}}}"
	cross_compile="${cross_compile:-${CROSS_COMPILE:-}}"
	build_dir="${BUILD_DIR:-$(pwd)/.build/${cross_compile%%-}/${build_target}}"
	keep_build_cache="${keep_build_cache:-${KEEP_BUILD_CACHE:-${DEF_KEEP_BUILD_CACHE}}}"
	output_dir="${output_dir:-${OUTPUT_DIR:-$(pwd)/${DEF_OUTPUT_DIR}/${cross_compile%%-}/${build_target}}}"

	init

	case "${@}" in
	*help*)
		build_project "help"
		cleanup
		exit 0
		;;
	*) ;;

	esac

	echo "Building for target '${build_target}'"
	build_project "${@}"

	if [ "${#}" -eq 0 ]; then
		echo "Installing into '${output_dir}'"
		build_project "install"
	fi

	echo "==============================================================================="
	echo "Build report for $(date -u)"
	echo
	build_project "size"
	echo "Successfully built for target '${build_target}' in $(($(date "+%s") - _start_time)) seconds."
	echo "==============================================================================="

	cleanup
}

main "${@}"

exit 0
