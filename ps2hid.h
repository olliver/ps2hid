/*
 * SPDX-license-Identifier: AGPL-3-or-later
 *
 * Copyright 2020 (C) Olliver Schinagl <oliver@schinagl.nl>
 */

#ifndef __PS2HID_H__
#define __PS2HID_H__ __FILE__

#if defined(__AVR_ATmega32U2__) || \
    defined(__AVR_ATmega16U2__) || \
    defined(__AVR_ATmega8U2__)
#define LED_DDR DDC5
#define LED_PORT PORTC5
#define LED_PIN PINC5

#elif defined(__AVR_ATmega32U4__) || \
      defined(__AVR_ATmega16U4__)
#define LED_DDR DDC7
#define LED_PORT PORTC7
#define LED_PIN PINC7

#else
#error "Attempting to compile for unsupported board."
#endif

#endif /* __PS2HID_H__ */
