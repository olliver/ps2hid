# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright 2020 (C) Olliver Schinagl <oliver@schinagl.nl>

BIN = ps2hid
PROGRAMMER ?= avr109
DEVICE ?= atmega32u4
CLOCK ?= 16000000UL
BAUD ?= 115200
PORT ?= /dev/arduino0
CROSS_COMPILE ?= avr-

OBJDIR ?= .build/$(DEVICE)
INSTALL_PATH ?= bin

AVRDUDE = avrdude -D -p $(DEVICE) -c $(PROGRAMMER) -P $(PORT)
CC = $(CROSS_COMPILE)gcc
CFLAGS = -std=c11 -Os -DF_CPU=$(CLOCK) -DBAUD=$(BAUD) -mmcu=$(DEVICE)
CFLAGS += \
          -Wall \
          -Walloc-zero \
          -Walloca \
          -Wbad-function-cast \
          -Wcast-align \
          -Wcast-qual \
          -Wconversion \
          -Wdeclaration-after-statement \
          -Wdouble-promotion \
          -Wduplicated-branches \
          -Wduplicated-cond \
          -Wextra \
          -Wfloat-equal \
          -Wimplicit-fallthrough=4 \
          -Wjump-misses-init \
          -Wlogical-op \
          -Wlong-long \
          -Wmissing-include-dirs \
          -Wmissing-prototypes \
          -Wnested-externs \
          -Wnull-dereference \
          -Wpacked \
          -Wredundant-decls \
          -Wshadow \
          -Wstack-protector \
          -Wstringop-truncation \
          -Wswitch-default \
          -Wswitch-enum \
          -Wuninitialized \
          -Wunsafe-loop-optimizations \
          -Wunsuffixed-float-constants \
          -Wunused-local-typedefs \
          -Wunused-macros \
          -Wwrite-strings

srcs = $(wildcard *.c)
objs = $(srcs:.c=.o)

all: $(BIN).hex
	@$(CROSS_COMPILE)size --format=berkley $(OBJDIR)/$(BIN).elf
	@echo "Done Compiling '$^'"

$(OBJDIR)/%.o: %.c
	@mkdir -p "$(OBJDIR)"
	$(CC) $(CFLAGS) $(LDFLAGS) -c -o $@ $<

$(OBJDIR)/$(BIN).elf: $(OBJDIR)/$(objs)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $<

.PHONY: size
size:
	@test -e $(OBJDIR)/$(BIN).elf && $(CROSS_COMPILE)size --format=sysv $(OBJDIR)/$(BIN).elf || true

$(BIN).hex: $(OBJDIR)/$(BIN).elf
	@rm -f $@
	$(CROSS_COMPILE)objcopy -j .text -j .data -O ihex $< $@

.PHONY: install
install: $(BIN).hex
	@install -D -m 644 -t $(INSTALL_PATH) $<

.PHONY: clean
clean:
	rm -rf $(BIN).hex $(OBJDIR)

flash: $(BIN).hex
	$(AVRDUDE) -U flash:w:$^:i

.PHONY: help
help:
	@echo 'Project builder'
	@echo '  clean		- Remove all generated binaries (hex, elf, *.o)'
	@echo '  all		- Compile all parts needed to generate the final hex'
	@echo '  install	- Copy final hex file into $$(INSTALL_PATH)'
	@echo '  size		- Print detailed object size information on an existing elf'
	@echo '  flash		- Compile and flash the hex file to the target'
	@echo
