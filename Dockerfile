# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2020 Olliver Schinagl <oliver@schinagl.nl>

FROM registry.hub.docker.com/library/alpine:latest

LABEL Maintainer="Olliver Schinagl <oliver@schinagl.nl>"

RUN apk add --no-cache \
        avr-libc \
        avrdude \
        gcc-avr \
        git \
        make \
    && \
    rm -rf /var/cache/apk/

ENV CROSS_COMPILE="avr-"
COPY "./dockerfiles/buildenv_check.sh" "/test/buildenv_check.sh"
