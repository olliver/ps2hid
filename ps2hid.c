/*
 * SPDX-license-Identifier: AGPL-3-or-later
 *
 * Copyright 2020 (C) Olliver Schinagl <oliver@schinagl.nl>
 */

#include <avr/io.h>
#include <avr/sleep.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <util/delay.h>

#include "ps2hid.h"

int main(void) {
	DDRC =  _BV(LED_DDR);
	PORTC = _BV(LED_PORT);

	while(true) { /* loop forever */
		PINC = _BV(LED_PIN);
		_delay_ms(250);
	}

	return 0;
}
